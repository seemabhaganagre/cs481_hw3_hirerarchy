﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hirearchy1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void B1_Clicked(object sender, EventArgs e) //this method is called when the button B1 is clicked  
        {
            await Navigation.PushAsync(new Page1());
        }

        private async void B2_Clicked(object sender, EventArgs e)  //this method is called when the button B2 is clicked
        {
            await Navigation.PushAsync(new Page2());
        }

        private async void B3_Clicked(object sender, EventArgs e)   //this method is called when the button B3 is clicked
        {
            await Navigation.PushAsync(new Page3());
        }
    }
}
