﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hirearchy1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }

        void OnSliderValueChanged(object sender, ValueChangedEventArgs args) // this method is invoked when we try to slide the slider 
        {
            double value = args.NewValue;
            sliderLabel.Scale = value;
            displayLabel.Text = String.Format("The Slider value is {0}", value);
        }

    }
}