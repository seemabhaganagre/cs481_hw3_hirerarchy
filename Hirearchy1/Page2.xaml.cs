﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hirearchy1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }

        private async void B4_Clicked(object sender, EventArgs e) //this method is invoked when the button b4 is clicked
        {
            await Navigation.PushAsync(new Page4());
        }
    }
}