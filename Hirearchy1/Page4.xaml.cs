﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hirearchy1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page4 : ContentPage
    {
        public Page4()
        {
            InitializeComponent();
        }
        private async void B5_Clicked(object sender, EventArgs e)  //this method takes me back to the root page and also shows a pop message. 
        {
            bool usersResponse = await DisplayAlert("Warning!", "Do not click me. Do you Still want to click me? ", "ok", "no");
            if (usersResponse == true)
            {
                await Navigation.PushAsync(new MainPage());
            }
        }
    }
}